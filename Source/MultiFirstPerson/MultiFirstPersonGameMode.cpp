// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "MultiFirstPersonGameMode.h"
#include "MultiFirstPersonHUD.h"
#include "MultiFirstPersonCharacter.h"
#include "UObject/ConstructorHelpers.h"

AMultiFirstPersonGameMode::AMultiFirstPersonGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AMultiFirstPersonHUD::StaticClass();
}
